This project is meant to contain text parsing tools.
So far, it contains two simple tools. The first one is "insert_spaces", a function that intelligently inserts spaces into texts that are missing spaces.
You can use the space insertion function as follows:

text='fourscore and sevenyears ago our fathersbrought forth on thiscontinent, a new nation, conceived inliberty, and dedicated to theproposition that allmen arecreated equal'

print(insertspaces(text))

The output will be:

four score and seven years ago our fathers brought forth on this continent a new nation conceived in liberty and dedicated to the proposition that all men are created equal

(the original text with spaces added back).

This space insertion tool is not perfect and is sensitive to the choice of corpus used.


The other tool it contains is a tool for search suggestions. It takes a search term and returns a related search term based on corpus n-gram frequencies.


You can use the search suggestion tool as follows:

print(search_suggestion('petroleum'))


This should give you the output:

petroleum tank

